package input

fun <T> advancedInput(bldr: AdvancedInputBuilder<T>.() -> Unit) = AdvancedInputBuilder<T>().apply(bldr).build()