package input

inline fun <T> input(message: String, conversion: (String) -> T) = print(message).run {
    conversion(readLine() as String)
}

fun input(message: String) = input(message) { message }
