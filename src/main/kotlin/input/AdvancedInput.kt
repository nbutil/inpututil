package input

import condition.Condition

class AdvancedInput<T>(
    private val inputConditions: List<Condition<T>>,
    private inline val getInputFunction: (String) -> String = {
        print(it)
        readLine() as String
    },
    private inline val onInputRetrievalException: (Exception) -> Unit = { println("EXCEPTION DURING INPUT RETRIEVAL: ${it.message}") },
    private inline val onInputConversionException: (Exception) -> Unit = { println("EXCEPTION DURING INPUT CONVERSION: ${it.message}")},
    inline val conversionFunction: (String) -> T
) {

    constructor(
        condition: Condition<T>,
        getInput: (String) -> String = {
            print(it)
            readLine() as String
        },
        onInputRetrievalException: (Exception) -> Unit = { println("EXCEPTION DURING INPUT RETRIEVAL: ${it.message}") },
        onInputConversionException: (Exception) -> Unit = { println("EXCEPTION DURING INPUT CONVERSION: ${it.message}")},
        conversion: (String) -> T
    ): this(
        inputConditions = listOf(condition),
        getInputFunction = getInput,
        onInputRetrievalException = onInputRetrievalException,
        onInputConversionException = onInputConversionException,
        conversionFunction = conversion
    )

    operator fun invoke(message: String): T {

        while (true) {
            val input = try {
                getInputFunction(message)
            } catch (e: Exception) {
                onInputRetrievalException(e)
                continue
            }

            val returnValue = try {
                conversionFunction(input)
            } catch (e: Exception) {
                onInputConversionException(e)
                continue
            }

            val failedCondition = inputConditions.firstOrNull { !it.test(returnValue) } ?: return returnValue
            failedCondition.failedTest(returnValue)
            println(failedCondition.getFailMessage(returnValue))
        }
    }
}
