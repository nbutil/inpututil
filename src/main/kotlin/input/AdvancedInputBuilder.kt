package input

import condition.Condition
import util.SingleSetVar

class AdvancedInputBuilder<T> {

    fun addCondition(condition: Condition<T>) { inputConditions.add(condition) }
    fun addConditions(vararg conditions: Condition<T>) { conditions.forEach { inputConditions.add(it) } }
    private val inputConditions: MutableList<Condition<T>> = mutableListOf()

    fun getInputFunction(func: (String) -> String) { getInputFunction = func }
    private var getInputFunction: (String) -> String by SingleSetVar {
        println(it)
        readLine() as String
    }

    fun onInputRetrievalException(func: (Exception) -> Unit) { onInputRetrievalException = func }
    private var onInputRetrievalException: (Exception) -> Unit by SingleSetVar {
        println("EXCEPTION DURING INPUT RETRIEVAL: ${it.message}")
    }

    fun onInputConversionException(func: (Exception) -> Unit) { onInputConversionException = func }
    private var onInputConversionException: (Exception) -> Unit by SingleSetVar {
        println("EXCEPTION DURING INPUT CONVERSION: ${it.message}")
    }

    fun convert(func: (String) -> T) { conversionFunction = func }
    private var conversionFunction: (String) -> T by SingleSetVar()

    fun build(): AdvancedInput<T> = AdvancedInput(
        inputConditions = inputConditions,
        getInputFunction = getInputFunction,
        onInputRetrievalException = onInputRetrievalException,
        onInputConversionException = onInputConversionException,
        conversionFunction = conversionFunction
    )
}