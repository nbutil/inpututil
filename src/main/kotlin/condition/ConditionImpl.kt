package condition

class ConditionImpl<T>(
    private inline val failedTestFunction: (T) -> Unit = {},
    private inline val failedTestMessage: (T) -> String,
    private inline val testFunction: (T) -> Boolean
): Condition<T> {
    override fun test(thing: T): Boolean = testFunction(thing)
    override fun failedTest(thing: T) = failedTestFunction(thing)
    override fun getFailMessage(thing: T): String = failedTestMessage(thing)
}
