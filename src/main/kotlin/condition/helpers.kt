package condition

fun <T> Condition(
    failedTestFunction: (T) -> Unit,
    failedTestMessage: (T) -> String,
    testFunction: (T) -> Boolean
) = ConditionImpl(
    failedTestFunction = failedTestFunction,
    failedTestMessage = failedTestMessage,
    testFunction = testFunction
)

fun <T> Condition(
    builder: ConditionBuilder<T>.() -> Unit
) = ConditionBuilder<T>().apply(builder).build()