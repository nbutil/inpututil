package condition

import util.SingleSetVar

class ConditionBuilder<T> {
    var testFunction: (T) -> Boolean by SingleSetVar()
    fun testFunction(function: (T) -> Boolean) { testFunction = function }

    var failedTestFunction: (T) -> Unit by SingleSetVar()
    fun failedTestFunction(function: (T) -> Unit) { failedTestFunction = function }

    var failedTestMessage: (T) -> String by SingleSetVar()
    fun failedTestMessage(function: (T) -> String) { failedTestMessage = function }

    fun build() = ConditionImpl(
        failedTestFunction = failedTestFunction,
        failedTestMessage = failedTestMessage,
        testFunction = testFunction
    )
}