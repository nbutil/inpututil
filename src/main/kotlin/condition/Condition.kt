package condition

interface Condition<T> {
    fun getFailMessage(thing: T): String
    fun test(thing: T): Boolean
    fun failedTest(thing: T)
}
